# Lancement rapide de l'application :
pip3 install -r requirements.txt

python3 main.py

# Dépendances nécessaires
fastapi==0.64.0
pytest==6.2.4
requests==2.25.1
uvicorn==0.13.4

# Description de l'application
Le serveur permet d'utiliser l'API DeckOfCards via deux commande simple à l'adresse locale http://localhost:8000 
- l'url "~/creer-un-deck/" permet de créer un nouveau jeu de carte (ou deck) et de récupérer son identifiant
- l'url "~/cartes/{nombre_cartes}" permet de tirer autant de cartes que voulu parmi le deck dont l'identifiant (deck_id) est fourni dans le corps de la requête http au format json. 