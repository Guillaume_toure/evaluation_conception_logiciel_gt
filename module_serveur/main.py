from fastapi import FastAPI

from pydantic import BaseModel

import requests

import uvicorn

app = FastAPI()


class Item(BaseModel):
    deck_id: str


@app.get("/creer-un-deck/")
async def creation_deck():
    requete = requests.get("https://deckofcardsapi.com/api/deck/new/\
                           shuffle/?deck_count=1")
    jiji = requete.json()
    id_deck = jiji["deck_id"]
    return(id_deck)


@app.post("/cartes/{nb_cartes}")
def exposer_cartes(nb_cartes: int, item: Item):
    requete = requests.get("https://deckofcardsapi.com/api/deck/" +
                           item.deck_id + "/draw/?count=" +
                           str(nb_cartes))
    jiji = requete.json()
    return(jiji)


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
