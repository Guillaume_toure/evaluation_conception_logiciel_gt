# Quick Start
Lancer le serveur sur un premier terminal :

    pip3 install -r module_serveur/requirements.txt

    python3 module_serveur/main.py

Lancer le client sur un second terminal :

    pip3 install -r module_client/requirements.txt

    python3 module_client/main.py

# Lancement des tests :
pip3 install -r module_client/requirements.txt

pytest

# Description de l'application
Le serveur permet d'utiliser l'API DeckOfCards via deux commande simple à l'adresse locale http://localhost:8000 
- l'url "~/creer-un-deck/" permet de créer un nouveau jeu de carte (ou deck) et de récupérer son identifiant
- l'url "~/cartes/{nombre_cartes}" permet de tirer autant de cartes que voulu parmi le deck dont l'identifiant (deck_id) est fourni dans le corps de la requête http au format json. 

L'application client permet de requêter l'API DeckOfCards via le serveur développé dans l'autre partie. Le programme principal main.py permet de réaliser successivement :
- La mise en place d'un jeu de 52 cartes via l'API DeckOfCards et retourne l'identifiant du jeu de carte (ou deck).
- Le tirage de 10 cartes dans le deck et retourne la liste des cartes tirées dans un dictionnaire au format json.
- Calculer le nombre de carte par couleur parmi les cartes tirées. 

