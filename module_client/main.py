import requests


def ouvrir_un_deck():
    requete = requests.get("http://localhost:8000/creer-un-deck/")
    nouveau_deck_id = requete.json()
    return(nouveau_deck_id)


def tirer_n_cartes(nb_cartes: int, deck_id: str):
    requete = requests.post("http://localhost:8000/cartes/" +
                            str(nb_cartes), json={'deck_id': deck_id},
                            headers={'Content-type': 'application/json',
                                     'Accept': 'text/plain'})
    return(requete.json())


def calcul_nb_carte(fichier_json: dict):
    mon_dico = {"H": 0, "S": 0, "D": 0, "C": 0}
    for carte in fichier_json["cards"]:
        couleur = carte["code"][1]
        if couleur in mon_dico.keys():
            mon_dico[couleur] = mon_dico[couleur] + 1
    return(mon_dico)


if __name__ == "__main__":
    r = ouvrir_un_deck()
    print(r)
    mon_deck_id = r
    r2 = tirer_n_cartes(10, mon_deck_id)
    print(r2)
    nb_cartes_tirees = calcul_nb_carte(r2)
    print(nb_cartes_tirees)
