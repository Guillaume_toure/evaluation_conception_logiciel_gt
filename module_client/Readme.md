# Lancement rapide de l'application :
pip3 install -r requirements.txt

python3 main.py

# Dépendances nécessaires
fastapi==0.64.0
pytest==6.2.4
flake8==3.9.0
pydantic==1.8.1
requests==2.25.1
uvicorn==0.13.4

# Lancement des tests :
pip3 install -r requirements.txt

pytest

# Description de l'application
L'application client permet de requêter l'API DeckOfCards via le serveur développé dans l'autre partie. Le programme principal main.py permet de réaliser successivement :
- La mise en place d'un jeu de 52 cartes via l'API DeckOfCards et retourne l'identifiant du jeu de carte (ou deck).
- Le tirage de 10 cartes dans le deck et retourne la liste des cartes tirées dans un dictionnaire au format json.
- Calculer le nombre de carte par couleur parmi les cartes tirées. 
