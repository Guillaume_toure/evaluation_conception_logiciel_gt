from main import calcul_nb_carte

import json


def test_calcul_nb_carte():
    with open('module_client/fichier_json_pourTest.json') as json_data:
        data_dict = json.load(json_data)
    assert calcul_nb_carte(data_dict) == {"H": 1, "S": 2, "D": 0, "C": 0}


if __name__ == "__main__":
    test_calcul_nb_carte()
